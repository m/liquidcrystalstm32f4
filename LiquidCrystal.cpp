#include "LiquidCrystal.h"



LiquidCrystal::LiquidCrystal(	Lcd_PortType ports[], uint16_t pins[],
								Lcd_PortType rs_port, uint16_t rs_pin,
								Lcd_PortType enable_port,uint16_t enable_pin,
								uint8_t mode)
{
	_lcd_ports=ports;
	_lcd_pins=pins;
	_rs_port=rs_port;
	_rs_pin=rs_pin;
	_enable_port = enable_port;
	_enable_pin=enable_pin;
	_lcd_mode=mode;
	init();
}

void LiquidCrystal::init(void) {
	if (_lcd_mode == LCD_4BITMODE)
		_displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
	else
		_displayfunction = LCD_8BITMODE | LCD_1LINE | LCD_5x8DOTS;

	begin(16, 1);
}

void LiquidCrystal::begin(uint8_t cols, uint8_t lines, uint8_t dotsize) {
	if (lines > 1) {
		_displayfunction |= LCD_2LINE;
	}
	_numlines = lines;

	setRowOffsets(0x00, 0x40, 0x00 + cols, 0x40 + cols);

	if ((dotsize != LCD_5x8DOTS) && (lines == 1)) {
		_displayfunction |= LCD_5x10DOTS;
	}

	HAL_Delay(50);
	//put the LCD into 4 bit or 8 bit mode
	if (!(_displayfunction & LCD_8BITMODE)) {
		// this is according to the hitachi HD44780 datasheet
		// figure 24, pg 46

		// we start in 8bit mode, try to set 4 bit mode
		write4bits(0x03);
		HAL_Delay(5); // wait min 4.1ms

		// second try
		write4bits(0x03);
		HAL_Delay(5); // wait min 4.1ms

		// third go!
		write4bits(0x03);
		HAL_Delay(1);

		// finally, set to 4-bit interface
		write4bits(0x02);
	} else {
		// this is according to the hitachi HD44780 datasheet
		// page 45 figure 23

		// Send function set command sequence
		command(LCD_FUNCTIONSET | _displayfunction);
		// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
		// according to datasheet, we need at least 40ms after power rises above 2.7V
		// before sending commands. Arduino can turn on way before 4.5V so we'll wait 50
		HAL_Delay(5);  // wait more than 4.1ms

		// second try
		command(LCD_FUNCTIONSET | _displayfunction);
		HAL_Delay(1);

		// third go
		command(LCD_FUNCTIONSET | _displayfunction);
	}

	// finally, set # lines, font size, etc.
	command(LCD_FUNCTIONSET | _displayfunction);

	// turn the display on with no cursor or blinking default
	_displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	display();

	// clear it off
	clear();

	// Initialize to default text direction (for romance languages)
	_displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	// set the entry mode
	command(LCD_ENTRYMODESET | _displaymode);
}

void LiquidCrystal::setRowOffsets(int row0, int row1, int row2, int row3) {
	_row_offsets[0] = row0;
	_row_offsets[1] = row1;
	_row_offsets[2] = row2;
	_row_offsets[3] = row3;
}

/********** high level commands, for the user! */
void LiquidCrystal::clear() {
	command(LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
	HAL_Delay(2);  // this command takes a long time!
}

void LiquidCrystal::home() {
	command(LCD_RETURNHOME);  // set cursor position to zero
	HAL_Delay(2);  // this command takes a long time!
}
void LiquidCrystal::setCursor(uint8_t row, uint8_t col) {
	const size_t max_lines = sizeof(_row_offsets) / sizeof(*_row_offsets);
	if (row >= max_lines) {
		row = max_lines - 1;    // we count rows starting w/0
	}
	if (row >= _numlines) {
		row = _numlines - 1;    // we count rows starting w/0
	}

	command(LCD_SETDDRAMADDR | (col + _row_offsets[row]));
}

// Turn the display on/off (quickly)
void LiquidCrystal::noDisplay() {
	_displaycontrol &= ~LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidCrystal::display() {
	_displaycontrol |= LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void LiquidCrystal::noCursor() {
	_displaycontrol &= ~LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidCrystal::cursor() {
	_displaycontrol |= LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void LiquidCrystal::noBlink() {
	_displaycontrol &= ~LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LiquidCrystal::blink() {
	_displaycontrol |= LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void LiquidCrystal::scrollDisplayLeft(void) {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void LiquidCrystal::scrollDisplayRight(void) {
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void LiquidCrystal::leftToRight(void) {
	_displaymode |= LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void LiquidCrystal::rightToLeft(void) {
	_displaymode &= ~LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void LiquidCrystal::autoscroll(void) {
	_displaymode |= LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void LiquidCrystal::noAutoscroll(void) {
	_displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | _displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void LiquidCrystal::createChar(uint8_t location, uint8_t charmap[]) {
	location &= 0x7; // we only have 8 locations 0-7
	command(LCD_SETCGRAMADDR | (location << 3));
	for (int i = 0; i < 8; i++) {
		write(charmap[i]);
	}
}

/*********** mid level commands, for sending data/cmds */
void LiquidCrystal::command(uint8_t value) {
	send(value, GPIO_PIN_RESET);
}


void LiquidCrystal::write(const std::string &s) {
	const char * string = s.c_str();
	for(uint8_t i = 0; i < s.length(); i++)
	{
		send(string[i], GPIO_PIN_SET);
	}
}

void LiquidCrystal::write(uint8_t value) {
	send(value, GPIO_PIN_SET);
}

void LiquidCrystal::write(int number)
{
	char buffer[11];
	sprintf(buffer, "%d", number);
	write(buffer);
}

void LiquidCrystal::write(float number)
{
	char buffer[11];
	snprintf(buffer, sizeof(buffer), "%f", number);
	write(buffer);
}

void LiquidCrystal::write(double number)
{
	char buffer[11];
	snprintf(buffer, sizeof(buffer), "%lf", number);
	write(buffer);
}

/************ low level data pushing commands **********/

// write either command or data, with automatic 4/8-bit selection
void LiquidCrystal::send(uint8_t value, GPIO_PinState State) {
	HAL_GPIO_WritePin(_rs_port, _rs_pin, State); // Write to command register

	if (_lcd_mode == LCD_4BITMODE) {
		write4bits((value >> 4));
		write4bits(value & 0x0F);
	} else {
		write8bits(value);
	}
}

void LiquidCrystal::pulseEnable(void) {
	HAL_GPIO_WritePin(_enable_port, _enable_pin, GPIO_PIN_RESET);
	HAL_Delay(1);
	HAL_GPIO_WritePin(_enable_port, _enable_pin, GPIO_PIN_SET);
	HAL_Delay(1);
	HAL_GPIO_WritePin(_enable_port, _enable_pin, GPIO_PIN_RESET);
	HAL_Delay(1);
}

void LiquidCrystal::write4bits(uint8_t data) {
	for (uint8_t i = 0; i < 4; i++) {
		HAL_GPIO_WritePin(_lcd_ports[i], _lcd_pins[i],
				static_cast<GPIO_PinState>((data >> i) & 0x01));
	}

	pulseEnable();
}

void LiquidCrystal::write8bits(uint8_t data) {
	for (uint8_t i = 0; i < 8; i++) {
		HAL_GPIO_WritePin(_lcd_ports[i], _lcd_pins[i],
				static_cast<GPIO_PinState>((data >> i) & 0x01));
	}

	pulseEnable();
}

LiquidCrystal::~LiquidCrystal() {
	// TODO Auto-generated destructor stub
}

